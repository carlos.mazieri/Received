import QtQuick 2.6

Timer {
    signal sleepTriggered()

    property int count: 0
    property int sleepSec: 0
    property int progress: sleepSec - count
    property string clockText: secToTimeString(sleepTimer.progress)

    id: sleepTimer
    running: false
    repeat: true
    onSleepTriggered: stopTimer()

    onTriggered: {
        count++;
        if(count >= sleepSec) {
            sleepTriggered()
        }
    }

    function stopTimer() {
        stop()
        count = 0;
    }

    function startTimer(sec) {
        console.log("Sleep timer set: " + sec);
        stopTimer();
        sleepSec = sec;
        start();
    }

    function secToTimeString(sec) {
        var hours = Math.floor(sec / 3600);
        var minutes = Math.floor((sec - (hours * 3600)) / 60);
        var seconds = sec - (hours * 3600) - (minutes * 60);

        hours = hours < 10 ? "0"+hours : hours;
        minutes = minutes < 10 ? "0"+minutes : minutes;
        seconds = seconds < 10 ? "0"+seconds : seconds;

        var time = hours+':'+minutes+':'+seconds;
        return time;
    }
}
