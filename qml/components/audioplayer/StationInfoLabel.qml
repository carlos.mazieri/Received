import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    id: stationInfoLabel
    spacing: 0

    property alias name: nameText.text
    property alias track: trackText.text

    Label {
        id: nameText
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeSmall
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        truncationMode: TruncationMode.Fade
        visible: text != ""

        width: parent.width
        height: parent.height / 2
    }

    Label {
        id: trackText
        color: Theme.secondaryColor
        font.pixelSize: Theme.fontSizeExtraSmall
        horizontalAlignment: Text.AlignLeft
        truncationMode: TruncationMode.Fade

        width: parent.width
        height: parent.height / 2
    }
}
