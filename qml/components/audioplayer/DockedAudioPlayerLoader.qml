import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0
import "../js/Favorites.js" as FavoritesUtils

DockedPanel {
    id: dockedAudioPlayerLoader

    property Item player

    // Layout
    width: parent.width
    dock: Dock.Bottom
    Behavior on opacity { FadeAnimation { duration: 250 } }
    Behavior on height { NumberAnimation { duration: 250 } }
    Behavior on y { }

    contentHeight: height
    flickableDirection: Flickable.VerticalFlick

    Loader {
        id: dockedAudioPlayer
        width: parent.width

        onLoaded: {
            dockedAudioPlayerLoader.player = dockedAudioPlayer.item
            dockedAudioPlayerLoader.height = dockedAudioPlayer.item.height
        }
    }

    // This timer is used when pushing 'next' or 'prev' buttons,
    // it does not try to play immediately, instead wait some time to allow the user
    // keep pushing, it is good for Bluetooth navigation inside cars
    Timer {
        id: navigation_timer
        interval: 1400
        running: false
        repeat: false
        onTriggered: playCurrentStation();
    }

    Component.onCompleted: {
        updateDockedAudioPlayerSource()
    }

    function updateDockedAudioPlayerSource() {
        dockedAudioPlayer.source = Qt.resolvedUrl(settings.value("playerLayout") + ".qml")
    }

    // Helper functions
    function play(station) {
        if (!expanded)
            open = true

        window.stationData = JSON.parse(JSON.stringify(station));
        AudioPlayer.setStation(window.stationData.name);
        AudioPlayer.setTitle("");
        if (navigation_timer.running == false) {
            playCurrentStation();
        }
    }

    function playNext() {
        var nextStation = FavoritesUtils.getNextFavorite(window.stationData);
        console.debug("Playing next favorite", JSON.stringify(nextStation));        
        navigation_timer.restart(); // allows keep pushing
        play(nextStation);
    }

    function playPrev() {
        var prevStation = FavoritesUtils.getPrevFavorite(window.stationData);
        console.debug("Playing prev favorite", JSON.stringify(prevStation));
        navigation_timer.restart(); // allows keep pushing
        play(prevStation);
    }

    function playCurrentStation() {
        AudioPlayer.loadUrl(window.stationData.url);
    }
}
