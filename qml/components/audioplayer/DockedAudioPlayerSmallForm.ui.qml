import QtQuick 2.6
import Sailfish.Silica 1.0

Item {
    id: player

    property bool isPlaying
    property bool isFavorite
    property string currentTrack
    property int bufferProgress
    property bool showProgressBar
    property bool sleepTimerActive
    property string sleepTimerText

    property alias opener: opener
    property alias buttonPlay: buttonPlay
    property alias playerMenu: playerMenu
    property alias timerSwitch: timerSwitch
    property alias favoriteSwitch: favoriteSwitch

    property real hightPadding: Theme.paddingMedium
    height: Theme.itemSizeSmall + (hightPadding * 2)

    MouseArea {
        id: opener
        anchors.fill: parent
    }

    Row {
        id: quickControlsItem
        anchors.fill: parent
        spacing: Theme.paddingLarge
        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.rightMargin: Theme.horizontalPageMargin
        anchors.topMargin: hightPadding
        anchors.bottomMargin: hightPadding

        Image {
            id: stationIcon
            height: parent.height
            width: parent.height

            source: window.stationData ? window.stationData.stationLogo : ""
            smooth: true
            cache: true

            fillMode: Image.PreserveAspectFit
            sourceSize.height: height
            sourceSize.width: height
        }

        StationInfoLabel {
            name: window.stationData ? window.stationData.name : ""
            track: currentTrack
            width: parent.width - (buttonPlay.width + stationIcon.width + Theme.paddingLarge*2)
            height: parent.height
        }

        IconButton {
            id: buttonPlay
            width: parent.height
            height: parent.height
            anchors.verticalCenter: parent.verticalCenter
            icon.source: isPlaying ? "image://theme/icon-m-pause" : "image://theme/icon-m-play"
        }
    }

    PushUpMenu {
        id: playerMenu
        width: parent.width

        Row {
            id: pushupMenuRow
            width: parent.width
            property real itemWidth: width / 2

            Switch {
                id: favoriteSwitch
                width: pushupMenuRow.itemWidth

                icon.source: "image://theme/icon-m-favorite"
                checked: isFavorite
            }

            SleepTimerSwitch {
                id: timerSwitch
                width: pushupMenuRow.itemWidth
                checked: sleepTimerActive
                showText: sleepTimerActive
                clockText: sleepTimerText
            }
        }
    }
}
