import QtQuick 2.6
import Sailfish.Silica 1.0

/**
 * Context menu intended for use within FavoritePage list
 *
 * index is coming from that list and is the index in the list that the contextmenu was triggered for
 */
ContextMenu {
    signal removeFavorite()
    signal editFavorite()

    MenuItem {
        text: qsTr("Remove from favorite")
        onClicked: {
            remorseAction(qsTr("Deleting"), function() {
                removeFavorite(index)
            });
        }
    }

    MenuItem {
        text: qsTr("Edit favorite")
        onClicked: editFavorite(index)
    }
}
