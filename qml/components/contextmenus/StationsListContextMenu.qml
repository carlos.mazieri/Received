import QtQuick 2.6
import Sailfish.Silica 1.0
import "../js/Favorites.js" as FavoritesUtils

/**
 * Context menu intended for use within StationsPage list
 *
 * index is coming from that list and is the index in the list that the contextmenu was triggered for
 */
ContextMenu {
    signal removeFavorite(var station)
    signal addFavorite(int id)

    property bool isFavorite: FavoritesUtils.isRadIoFavorite(id);

    MenuItem {
        text: qsTr("Remove from favorite")
        visible: isFavorite
        onClicked: {
            remorseAction(qsTr("Deleting"), function() {
                removeFavorite(FavoritesUtils.getFavoriteByRadioId(id))
                isFavorite = false
            });
        }
    }

    MenuItem {
        text: qsTr("Add to favorite")
        visible: !isFavorite
        onClicked: {
            addFavorite(id)
            isFavorite = true
        }
    }
}
