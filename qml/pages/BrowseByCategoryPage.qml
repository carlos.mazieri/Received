import QtQuick 2.6
import "../components/js/Utils.js" as Utils

BrowseByCategoryPageForm {

    headerTitle: headerTitle.toLocaleLowerCase()

    browseByListView.onCurrentIndexChanged: {
        var item = browseByModel.get(browseByListView.currentIndex)
        pageStack.push(Qt.resolvedUrl("StationsPage.qml"), {listType: new Utils.ListType(item.title),
                           category: item.category, value: item.title});
    }


    // Other

    // Set a custom search action in navigation menu
    navigationMenu.searchAction: function() {
        pageStack.replaceAbove(
                    pageStack.find(function(page) {
                        return page.firstPage === true}),
                    Qt.resolvedUrl("StationsPage.qml"),
                    {listType: Utils.Search});
    }

    Component.onCompleted: {
        radioAPI.getCategories(category.toLocaleLowerCase(), browseByModel);
    }
}
