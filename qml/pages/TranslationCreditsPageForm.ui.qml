import QtQuick 2.6
import Sailfish.Silica 1.0

import "../components"
import "../components/listmodels/"

Page {
    id: page

    SilicaListView {
        id: listView
        VerticalScrollDecorator { flickable: listView }
        anchors.fill: parent

        header: Column {
            PageHeader {
                title: qsTr("Translations")
                width: page.width
            }
        }

        model: TranslationCreditsListModel {}
        delegate: TranslationCreditsDeligate {}
    }
}
