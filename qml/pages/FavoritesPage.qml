import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components/js/Favorites.js" as FavoritesUtils

FavoritesPageForm {

    // Recives index as parameter from signal
    onPlayFavorite: {
        var station = getStation(index);
        console.debug("Playing", JSON.stringify(station))
        player.play(station)
    }

    // Recives index as parameter from signal
    onRemoveFavorite: FavoritesUtils.removeFavorite(getStation(index));

    onEditFavorite: {
        var station = getStation(index);
        var dialog = pageStack.push(Qt.resolvedUrl("FavoritePage.qml"),
                                                        {station: station})
        handleCustomFavorite(dialog);
    }

    addCustomFavorite.onClicked: {
        var dialog = pageStack.push(Qt.resolvedUrl("FavoritePage.qml"))
        handleCustomFavorite(dialog);
    }

    function getStation(index) {
        return favoriteModel.get(index);
    }

    function handleCustomFavorite(dialog) {
        dialog.accepted.connect(function() {
            console.log(JSON.stringify(dialog.station))
            FavoritesUtils.addFavorite(dialog.station)
        })
    }

    Component.onCompleted: FavoritesUtils.init(favoriteModel)
}
