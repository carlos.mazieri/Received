import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
    id: customFavoriteDialog

    property var station

    property alias url: urlField.text
    property alias name: nameField.text
    property alias logo: logoField.text
    property alias country: countryField.text
    property alias genre: genreField.text

    canAccept: url && name

    SilicaFlickable {
        id: flickable
        anchors.fill: parent
        contentHeight: layout.height
        VerticalScrollDecorator { flickable: flickable }

        Column {
            id: layout
            width: parent.width

            DialogHeader { }

            TextField {
                id: urlField
                width: parent.width
                placeholderText: qsTr("http://mystation.com/stream.mp3")
                text: station ? station.url : ""
                label: qsTr("URL")
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: nameField.focus = true
            }

            TextField {
                id: nameField
                width: parent.width
                placeholderText: qsTr("My Station")
                text: station ? station.name : ""
                label: qsTr("Name")
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: logoField.focus = true
            }

            TextField {
                id: logoField
                width: parent.width
                placeholderText: qsTr("http://mystation.com/logo.jpg")
                text: station ? station.stationLogo : ""
                label: qsTr("Logo")
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: countryField.focus = true
            }

            TextField {
                id: countryField
                width: parent.width
                placeholderText: qsTr("Sweden")
                text: station ? station.country : ""
                label: qsTr("Country")
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: genreField.focus = true
            }

            TextField {
                id: genreField
                width: parent.width
                placeholderText: qsTr("Pop")
                text: station ? station.genre : ""
                label: qsTr("Genre")
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: customFavoriteDialog.accept()
            }
        }
    }
}
