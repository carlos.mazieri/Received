<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>AboutPageForm.ui</name>
    <message>
        <source>About Received</source>
        <translation>Om Received</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>Översättningar</translation>
    </message>
    <message>
        <source>Report an Issue</source>
        <translation>Rapportera ett problem</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
</context>
<context>
    <name>ApiLanguageListModel</name>
    <message>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <source>German</source>
        <translation>Tyska</translation>
    </message>
    <message>
        <source>Austrian</source>
        <translation>Österrike</translation>
    </message>
    <message>
        <source>French</source>
        <translation>Frankrike</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation>Portugal</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation>Spanien</translation>
    </message>
</context>
<context>
    <name>BrowseByCategoryPageForm.ui</name>
    <message>
        <source>Browse by</source>
        <translation>Bläddra efter</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Local</source>
        <translation>Lokalt</translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation>Topp 100</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Rekommenderade</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>Ämne</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Stad</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
</context>
<context>
    <name>BrowsePageForm.ui</name>
    <message>
        <source>Browse</source>
        <translation>Bläddra</translation>
    </message>
</context>
<context>
    <name>DockedAudioPlayerForm.ui</name>
    <message>
        <source>Sleep timer</source>
        <translation>insomningstimer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>Ställ</translation>
    </message>
</context>
<context>
    <name>FavoritePageForm.ui</name>
    <message>
        <source>http://mystation.com/stream.mp3</source>
        <translation>http://minstation.se/kanal.mp3</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>My Station</source>
        <translation>Min Station</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <source>http://mystation.com/logo.jpg</source>
        <translation>http://minstation.se/logo.jpg</translation>
    </message>
    <message>
        <source>Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <source>Sweden</source>
        <translation>Sverige</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <source>Pop</source>
        <translation>Pop</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
</context>
<context>
    <name>FavoritesListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Ta bort från favoriter</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Tar bort</translation>
    </message>
    <message>
        <source>Edit favorite</source>
        <translation>Redigera favorit</translation>
    </message>
</context>
<context>
    <name>FavoritesPageForm.ui</name>
    <message>
        <source>Favorites</source>
        <translation>Favoriter</translation>
    </message>
    <message>
        <source>From </source>
        <translation>Från</translation>
    </message>
    <message>
        <source>Add Custom</source>
        <translation>Lägg till egen</translation>
    </message>
</context>
<context>
    <name>NavigationMenuForm.ui</name>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Show player</source>
        <translation>Visa spelare</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>Bläddra</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
</context>
<context>
    <name>PlayerLayoutListModel</name>
    <message>
        <source>Original player</source>
        <translation>Orginal</translation>
    </message>
    <message>
        <source>Small player</source>
        <translation>Liten</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Droping DB</source>
        <translation>Raderar databsen</translation>
    </message>
</context>
<context>
    <name>SettingsPageForm.ui</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Advanced Options</source>
        <translation>Avancerade inställningar</translation>
    </message>
    <message>
        <source>Reset DB</source>
        <translation>Ränsa databasen</translation>
    </message>
    <message>
        <source>API Language:</source>
        <translation>API språk</translation>
    </message>
    <message>
        <source>Player layout</source>
        <translation>Spelar Utseende</translation>
    </message>
    <message>
        <source>API Options</source>
        <translation>API Inställningar</translation>
    </message>
    <message>
        <source>Basic Options</source>
        <translation>Standard Inställningar</translation>
    </message>
    <message>
        <source>Sets what style to use for player controls</source>
        <translation>Sätter vilken stil som ska användas för spelarkontrollerna</translation>
    </message>
    <message>
        <source>Sets the endpoint to be used for API calls e.g. rad.io for English and radio.de for German</source>
        <translation>Sätter vilken API url som ska användas. t.ex rad.io för engelska och radio.de för tyska</translation>
    </message>
    <message>
        <source>Removes everything in the database and gives you a clean start. &lt;i&gt;&lt;b&gt;Used with caution&lt;/b&gt;&lt;/i&gt;</source>
        <translation>Ränsar databasen och ger dig en ny start. &lt;i&gt;&lt;b&gt;Används med försiktighet&lt;/b&gt;&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>StationsListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Ta bort från favoriter</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Tar bort</translation>
    </message>
    <message>
        <source>Add to favorite</source>
        <translation>Lägg till i favoriter</translation>
    </message>
</context>
<context>
    <name>StationsPageForm.ui</name>
    <message>
        <source>From</source>
        <translation>Från</translation>
    </message>
</context>
<context>
    <name>TranslationCreditsListModel</name>
    <message>
        <source>French</source>
        <translation>Franska</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation>Ryska</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation>Spanska</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
</context>
<context>
    <name>TranslationCreditsPageForm.ui</name>
    <message>
        <source>Translations</source>
        <translation>Översättningar</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>Top 100</source>
        <translation>Topp 100</translation>
    </message>
    <message>
        <source>Search station</source>
        <translation>Sök station</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favoriter</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Rekommenderade</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Lokalt</translation>
    </message>
</context>
</TS>
