#!/bin/bash

# Helper script to create html formated change logs
# 
# usage:
#	./pretty_git_log commit/branch#1 commit/branch#2
#
# example:
#	./pretty_git_log release-v1.0.0 release-v1.0.1

git log $1...$2 --pretty=format:'<li> <a href="https://gitlab.com/sailfish-apps/Received/commit/%H">view commit &bull;</a> %s</li> ' | grep -v "Merge"
